
# codechallenge-java Jr

**CODE CHALLENGE**

Este code challenge permite mostrar los conocimientos de back. 

Estos son los requerimiento para la prueba :
- Desarrollar un backend (RESTful) con Java usando la definicón Swagger disponible en la siguiente URL https://s3-eu-west-1.amazonaws.com/mmi-codechallenge/swagger-users-v1.json
- Usar Java 11 o superior con los frameworks Spring Boot 2.7 o superior, Spring MVC, Spring JPA.
- El API permitirá dar de alta, modificar, eliminar y consultar usuarios. 
- El API tiene que cumplir los recursos definidos en el Swagger.

Por otro lado, se valorará:
1) Realización de pruebas unitarias.
2) Realización de pruebas de integración


**ENTREGA**

El tiempo de desarrollo es de una semana pero mucho mejor si nos lo puedes enviar antes.


Se debe entregar:

- Url con la aplicación desplegada.
- Código fuente en el repositorio de gitlab adjunto (No comprimido, haciendo uso correcto de git)
- Postman de prueba.

**RECUERDA**

- La evaluación la realizarán nuestros ninja masters, gente con mucha experiencia.
- Según tu calificación, recibirás un diploma de ninja

