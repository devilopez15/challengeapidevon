package com.challenge.ninja.controllers;

import com.challenge.ninja.models.User;
import com.challenge.ninja.models.dtoModels.AddressDto;
import com.challenge.ninja.models.dtoModels.UserDto;
import com.challenge.ninja.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/getusers")
    public ResponseEntity<List<UserDto>> getUsers() {
        List<User> users = userService.getAllUsers();
        List<UserDto> userDtos = users.stream()
                .map(User::toDto)
                .collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(userDtos);
    }

    @PostMapping("/createUsers")
    public ResponseEntity<UserDto> createUsers(@RequestBody UserDto userDto) {
        if (userDto == null || userDto.getName() == null || userDto.getEmail() == null || userDto.getBirthDate() == null || userDto.getAddress() == null) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
                    .body(null);
        }

        User user = User.fromDto(userDto);
        User createdUser = userService.createUser(user);
        UserDto createdUserDto = createdUser.toDto();
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUserDto);
    }

    @GetMapping("/getusersById/{userId}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Integer userId) {
        if (userId <= 0) {
            return ResponseEntity.badRequest().build();
        }

        Optional<User> user = userService.findUserById(userId);

        if (user.isPresent()) {
            UserDto userDto = user.get().toDto();
            return ResponseEntity.ok(userDto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/updateUsersById/{userId}")
    public ResponseEntity<UserDto> updateUserById(@PathVariable Integer userId, @RequestBody UserDto userDto) {
        Optional<User> existingUser = userService.findUserById(userId);

        if (existingUser.isPresent()) {
            User user = User.fromDto(userDto);
            AddressDto addressDto = userDto.getAddress();

            User updatedUser = userService.updateUserWithAddress(userId, user, addressDto);

            if (updatedUser != null) {
                UserDto updatedUserDto = updatedUser.toDto();
                return ResponseEntity.ok(updatedUserDto);
            } else {
                return ResponseEntity.badRequest().build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }



    @DeleteMapping("/deleteUsersById/{userId}")
    public ResponseEntity<Void> deleteUsersById(@PathVariable Integer userId) {
        if(!userService.findUserById(userId).isPresent()) {
            return ResponseEntity.notFound().build();
        }
        boolean isDeleted = userService.deleteUserById(userId);

        if (isDeleted) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}