package com.challenge.ninja.services;

import com.challenge.ninja.models.Address;
import com.challenge.ninja.models.User;
import com.challenge.ninja.models.dtoModels.AddressDto;
import com.challenge.ninja.repository.AddressRepository;
import com.challenge.ninja.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;

    public UserService(UserRepository repository, AddressRepository addressRepository) {
        this.userRepository = repository;
        this.addressRepository = addressRepository;
    }

    public List<User> getAllUsers() {
        List<User> users = userRepository.findAll();

        return users;

    }

    public User createUser(User user) {
        Address address = user.getAddress();

        if (address != null) {
            Address savedAddress = addressRepository.save(address);
            user.setAddress(savedAddress);
        }

        return userRepository.save(user);
    }

    public Optional<User> findUserById(Integer userId) {
        return userRepository.findById(userId);
    }

    public User updateUser(Integer userId, User user) {
        Optional<User> existingUser = userRepository.findById(userId);

        if (existingUser.isPresent()) {
            User updatedUser = existingUser.get();
            updatedUser.setName(user.getName());
            updatedUser.setEmail(user.getEmail());
            updatedUser.setBirthDate(user.getBirthDate());
            updatedUser.setAddress(user.getAddress());

            return userRepository.save(updatedUser);
        } else {
            throw new EntityNotFoundException("User not found");
        }
    }

    public User updateUserWithAddress(Integer userId, User user, AddressDto addressDto) {
        Optional<User> existingUser = userRepository.findById(userId);

        if (existingUser.isPresent()) {
            User updatedUser = existingUser.get();
            updatedUser.setName(user.getName());
            updatedUser.setEmail(user.getEmail());
            updatedUser.setBirthDate(user.getBirthDate());

            if (addressDto != null) {
                Address address = Address.fromDto(addressDto);

                if (address.getId() == null) {
                    Address savedAddress = addressRepository.save(address);
                    updatedUser.setAddress(savedAddress);
                } else {
                    Optional<Address> existingAddress = addressRepository.findById(address.getId());
                    existingAddress.ifPresent(savedAddress -> {
                        savedAddress.setStreet(address.getStreet());
                        savedAddress.setState(address.getState());
                        savedAddress.setCity(address.getCity());
                        savedAddress.setCountry(address.getCountry());
                        savedAddress.setZip(address.getZip());
                        updatedUser.setAddress(savedAddress);
                    });
                }
            }

            return userRepository.save(updatedUser);
        } else {
            return null;
        }
    }

    public boolean deleteUserById(Integer id) {
        if (id == null || id <= 0) {
            return false;
        }

        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
            return true;
        }
        return false;
    }

}
