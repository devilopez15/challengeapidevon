package com.challenge.ninja.models;

import com.challenge.ninja.models.dtoModels.UserDto;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.beans.BeanUtils;

import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String email;

    private LocalDateTime birthDate;

    @ManyToOne
    @JoinColumn(name = "address_id")
    private Address address;

    public User(Integer id, String name, String email, LocalDateTime birthDate, Address address) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.birthDate = birthDate;
        this.address = address;
    }

    public User() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public UserDto toDto() {
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(this, userDto, "address");
        if (this.address != null) {
            userDto.setAddress(this.address.toDto());
        }
        return userDto;
    }

    public static User fromDto(UserDto userDto) {
        User user = new User();
        BeanUtils.copyProperties(userDto, user, "userAddress");
        if (userDto.getAddress() != null) {
            user.setAddress(Address.fromDto(userDto.getAddress()));
        }
        return user;
    }


}


