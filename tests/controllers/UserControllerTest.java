package controllers;

import com.challenge.ninja.ChallengeApiApplication;
import com.challenge.ninja.controllers.UserController;
import com.challenge.ninja.models.Address;
import com.challenge.ninja.models.User;
import com.challenge.ninja.models.dtoModels.AddressDto;
import com.challenge.ninja.models.dtoModels.UserDto;
import com.challenge.ninja.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ChallengeApiApplication.class)
@AutoConfigureMockMvc
public class UserControllerTest {

    UserController userController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    @DisplayName("Integration - FindAll")
    public void testFindAllUsers() throws Exception {
        List<User> users = new ArrayList<>();
        Address address = new Address(1, "street", "state", "city", "country", "zip");
        LocalDateTime birthDate = LocalDateTime.of(1990, 1, 15, 0, 0);
        User user = new User(1, "name", "email", birthDate, address);
        users.add(user);

        given(userService.getAllUsers()).willReturn(users);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/getusers")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers
                        .content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

    }

    @Test
    @DisplayName("Integration - CreateUser")
    public void testCreateUser() throws Exception {
        AddressDto addressDto = new AddressDto();
        addressDto.setStreet("Street");
        addressDto.setState("State");
        addressDto.setCity("City");
        addressDto.setCountry("Country");
        addressDto.setZip("12345");

        UserDto userDto = new UserDto();
        userDto.setName("Devon");
        userDto.setEmail("dlopez@example.com");
        userDto.setBirthDate(LocalDateTime.of(1990, 1, 15, 0, 0));
        userDto.setAddress(addressDto);

        User createdUser = new User(1, "Devon", "dlopez@example.com", userDto.getBirthDate(), new Address());
        given(userService.createUser(any(User.class))).willReturn(createdUser);

        mockMvc.perform(MockMvcRequestBuilders.post("/users/createUsers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userDto)))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers
                        .content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        verify(userService, times(1)).createUser(any(User.class));
    }

    @Test
    @DisplayName("Integration - Get User by ID")
    public void testGetUserById() throws Exception {
        Address address = new Address(1, "Street", "State", "City", "Country", "12345");
        LocalDateTime birthDate = LocalDateTime.of(1990, 1, 15, 0, 0);
        User user = new User(1, "Devon", "Devon@example.com", birthDate, address);

        given(userService.findUserById(1)).willReturn(Optional.of(user));

        mockMvc.perform(MockMvcRequestBuilders.get("/users/getusersById/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers
                        .content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    @DisplayName("Integration - Get User by ID (User Not Found)")
    public void testGetUserByIdNotFound() throws Exception {
        given(userService.findUserById(999)).willReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/getusersById/999")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }


/*
    @Test
    @DisplayName("Integration - Update User by ID")
    public void testUpdateUserById() throws Exception {
        Address address = new Address(1, "Street", "State", "City", "Country", "12345");
        LocalDateTime birthDate = LocalDateTime.of(1990, 1, 15, 0, 0);
        User user = new User(1, "Devon", "Devon@example.com", birthDate, address);
        UserDto updatedUserDto = user.toDto();
        updatedUserDto.setName("Updated Name");

        given(userService.findUserById(1)).willReturn(Optional.of(user));
        given(userService.updateUserWithAddress(1, user, updatedUserDto.getAddress())).willReturn(user);

        String updatedUserJson = objectMapper.writeValueAsString(updatedUserDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/users/updateUsersById/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedUserJson))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers
                        .content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
*/

    @Test
    @DisplayName("Integration - Update User by ID (User Not Found)")
    public void testUpdateUserByIdNotFound() throws Exception {
        UserDto updatedUserDto = new UserDto();
        updatedUserDto.setName("Updated Name");

        given(userService.findUserById(999)).willReturn(Optional.empty());

        String updatedUserJson = objectMapper.writeValueAsString(updatedUserDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/users/updateUsersById/999")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedUserJson))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Integration - Delete User by ID")
    public void testDeleteUserById() throws Exception {
        given(userService.findUserById(1)).willReturn(Optional.of(new User()));
        given(userService.deleteUserById(1)).willReturn(true);

        mockMvc.perform(delete("/users/deleteUsersById/1"))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Integration - Delete User by ID (Not Found)")
    public void testDeleteUserByIdNotFound() throws Exception {
        given(userService.findUserById(1)).willReturn(Optional.empty());
        given(userService.deleteUserById(1)).willReturn(false);

        mockMvc.perform(delete("/users/deleteUsersById/1"))
                .andExpect(status().isNotFound());
    }




}
