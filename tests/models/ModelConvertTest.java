package models;

import com.challenge.ninja.models.Address;
import com.challenge.ninja.models.User;
import com.challenge.ninja.models.dtoModels.AddressDto;
import com.challenge.ninja.models.dtoModels.UserDto;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;


public class ModelConvertTest {
    @Test
    public void testUserToDto() {
        Address address = new Address(1, "Street", "State", "City", "Country", "12345");
        LocalDateTime birthDate = LocalDateTime.of(1990, 1, 15, 0, 0);

        User user = new User(1, "John Doe", "john@example.com", birthDate, address);

        UserDto userDto = user.toDto();

        assertNotNull(userDto);
        assertEquals(user.getName(), userDto.getName());
        assertEquals(user.getEmail(), userDto.getEmail());
        assertEquals(user.getBirthDate(), userDto.getBirthDate());

        AddressDto userAddressDto = userDto.getAddress();
        assertNotNull(userAddressDto);
        assertEquals(address.getStreet(), userAddressDto.getStreet());
        assertEquals(address.getState(), userAddressDto.getState());
        assertEquals(address.getCity(), userAddressDto.getCity());
        assertEquals(address.getCountry(), userAddressDto.getCountry());
        assertEquals(address.getZip(), userAddressDto.getZip());

    }

    @Test
    public void testAddressToDto() {
        Address address = new Address(1, "Street", "State", "City", "Country", "12345");

        AddressDto addressDto = address.toDto();

        assertNotNull(addressDto);
        assertEquals(address.getStreet(), addressDto.getStreet());
        assertEquals(address.getState(), addressDto.getState());
        assertEquals(address.getCity(), addressDto.getCity());
        assertEquals(address.getCountry(), addressDto.getCountry());
        assertEquals(address.getZip(), addressDto.getZip());
    }

    @Test
    public void testAddressFromDto() {
        AddressDto addressDto = new AddressDto();
        addressDto.setStreet("Main St");
        addressDto.setState("CA");
        addressDto.setCity("Los Angeles");
        addressDto.setCountry("USA");
        addressDto.setZip("12345");

        Address convertedAddress = Address.fromDto(addressDto);

        assertNotNull(convertedAddress);
        assertEquals(addressDto.getStreet(), convertedAddress.getStreet());
        assertEquals(addressDto.getState(), convertedAddress.getState());
        assertEquals(addressDto.getCity(), convertedAddress.getCity());
        assertEquals(addressDto.getCountry(), convertedAddress.getCountry());
        assertEquals(addressDto.getZip(), convertedAddress.getZip());
    }

    @Test
    public void testUserFromDto() {
        UserDto userDto = new UserDto();
        userDto.setName("John Doe");
        userDto.setEmail("john@example.com");
        userDto.setBirthDate(LocalDateTime.of(1990, 1, 15, 0, 0));

        AddressDto addressDto = new AddressDto();
        userDto.setAddress(addressDto);

        User convertedUser = User.fromDto(userDto);

        assertNotNull(convertedUser);
        assertEquals(userDto.getName(), convertedUser.getName());
        assertEquals(userDto.getEmail(), convertedUser.getEmail());
        assertEquals(userDto.getBirthDate(), convertedUser.getBirthDate());

    }

}
