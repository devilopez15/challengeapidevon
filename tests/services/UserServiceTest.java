package services;

import com.challenge.ninja.models.Address;
import com.challenge.ninja.models.User;
import com.challenge.ninja.repository.AddressRepository;
import com.challenge.ninja.repository.UserRepository;
import com.challenge.ninja.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private AddressRepository addressRepository;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User());

        when(userRepository.findAll()).thenReturn(users);

        List<User> result = userService.getAllUsers();

        assertNotNull(result);
        assertEquals(users.get(0), result.get(0));
        assertEquals(users.size(), result.size());

        verify(userRepository, times(1)).findAll();
    }

    @Test
    public void testCreateUser() {
        User user = new User();
        Address address = new Address();
        user.setAddress(address);

        when(addressRepository.save(any(Address.class))).thenReturn(address);
        when(userRepository.save(user)).thenReturn(user);

        User createdUser = userService.createUser(user);

        assertNotNull(createdUser);
        assertEquals(address, createdUser.getAddress());

        verify(addressRepository, times(1)).save(address);
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void testFindUserById() {
        User user = new User();
        int userId = 1;

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        Optional<User> result = userService.findUserById(userId);

        assertTrue(result.isPresent());
        assertEquals(user, result.get());

        verify(userRepository, times(1)).findById(userId);
    }

    @Test
    public void testUpdateUser() {
        int userId = 1;
        User existingUser = new User();
        existingUser.setId(userId);

        User updatedUser = new User();
        updatedUser.setId(userId);
        updatedUser.setName("Updated Name");

        when(userRepository.findById(userId)).thenReturn(Optional.of(existingUser));
        when(userRepository.save(any())).thenReturn(updatedUser);

        User result = userService.updateUser(userId, updatedUser);

        assertNotNull(result);
        assertEquals(updatedUser.getName(), result.getName());

        verify(userRepository, times(1)).findById(userId);
        verify(userRepository, times(1)).save(any());
    }

    @Test
    public void testDeleteUserById() {
        int userId = 1;

        when(userRepository.existsById(userId)).thenReturn(true);

        boolean isDeleted = userService.deleteUserById(userId);

        assertTrue(isDeleted);

        verify(userRepository, times(1)).existsById(userId);
        verify(userRepository, times(1)).deleteById(userId);
    }
}
